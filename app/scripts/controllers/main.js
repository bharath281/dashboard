'use strict';

/**
 * @ngdoc function
 * @name voteApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the voteApp
 */
angular.module('voteApp')
  .controller('MainCtrl', function ($scope,$rootScope,$uibModal,userService,$window) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  $scope.page = 1;
	$rootScope.login=false;
  $scope.global = $rootScope;
  $scope.loader = false;
  $scope.questionrefresh = false;
	var token = localStorage.getItem('token');
	if(token){
    $rootScope.name = localStorage.getItem('name');
		$rootScope.login=true;
	}else{
    $rootScope.name = '';
    $rootScope.login=false;
  }
    $scope.loginModel = function(){
      $rootScope.modalInstance = $uibModal.open({
        templateUrl: 'views/login.html',  
        controller: "ModalCtrl",
        scope:$scope 
      });
    };

    $scope.signUpModel = function(){
      $rootScope.modalInstance = $uibModal.open({
        templateUrl: 'views/signup.html',  
        controller: "ModalCtrl",
        scope:$scope 
      });
    };

    $scope.forgotModel = function(){
      $rootScope.modalInstance = $uibModal.open({
        templateUrl: 'views/forgot.html',  
        controller: "ModalCtrl",
        scope:$scope 
      });
    };

	$scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    }
    // $scope.commentModel = function(id){
    //   if($rootScope.login){
    //     $scope.comment = {};
    //     $scope.comment.question_id = id;
    //     $rootScope.modalInstance = $uibModal.open({
    //       templateUrl: 'views/comment.html',  
    //       controller: "ModalCtrl",
    //       scope:$scope
    //     });
    //   }else{
    //      $rootScope.showCustomToast('Please login to add a comment');
    //   }
    // };

    $scope.viewCommentModel = function(id){
      if($rootScope.login){
         $scope.comment = {};
         $scope.comment.question_id = id;
        userService.getComments(id).then(function(comments){
          $scope.comments = comments;
          $rootScope.modalInstance = $uibModal.open({
            templateUrl: 'views/comment.html',  
            controller: "ModalCtrl",
            scope:$scope
          });
        },function(err){
          $scope.comments = [];
          $rootScope.showCustomToast(err);
        })
      }else{
         $rootScope.showCustomToast('Please login to view a comment');
      }
    };


    $scope.logOut = function(){
      	$window.localStorage.removeItem('token');
      	$rootScope.login=false;
    };

    $scope.questions = [];
    $scope.nexPage = false;
    $scope.getQuesions= function(){
      $scope.questionrefresh = true;
    	userService.getQuesions($scope.page).then(function(data){
        $scope.manupulateQuestions(data.questions);
    	},function(err){
    		$rootScope.showCustomToast(err.message);
    	})
    };


    $scope.manupulateQuestions = function(data){
        $scope.quesionsData = data;
        if(!data.hasOwnProperty('message')){
          if($scope.questions && $scope.questions.length>0){
            $scope.questions.push.apply($scope.questions,data.data);
          }else if(data.data.length>0){
            $scope.questions = data.data;
          }
          if(data.next_page_url){
            $scope.nexPage = true;
          }else{
            $scope.nexPage = false;
          }
        }else{
          $rootScope.showCustomToast('There is no question available.');
        }
        $scope.questionrefresh = false;
    }

    $scope.getMenu= function(){
    	userService.getMenu().then(function(data){
    		$scope.menuData = data;
    	},function(err){
    		$rootScope.showCustomToast(err.message);
    	})
    };

    $scope.addQuesion = function(){
      if($rootScope.login){
        $rootScope.modalInstance = $uibModal.open({
          templateUrl: 'views/addquesion.html',  
          controller: "ModalCtrl",
          scope:$scope 
        });
      }else{

        $rootScope.modalInstance = $uibModal.open({
          templateUrl: 'views/login.html',  
          controller: "ModalCtrl",
          scope:$scope 
        });
        //$rootScope.showCustomToast('Please login to add question');
      }
    };

    $scope.nextPage = function(){
      $scope.page +=1;
      $scope.getQuesions();
    };

    $scope.getQuesions();
    $scope.getMenu();

    $scope.getMenuQuestions = function(id){
      $scope.questionrefresh = true;
      userService.getMenuQuestions(id).then(function(data){
        $scope.questions = [];
        $scope.nexPage = false;
        $scope.manupulateQuestions(data);
      },function(err){
        console.log(err);
      })
    }
    $scope.currentNavItem = '';
    $scope.$watch('currentNavItem', function() {
      if($scope.currentNavItem != ''){
        $scope.getMenuQuestions($scope.currentNavItem)
      }
    });

    $scope.myQuestions = function(){
      userService.getMyQuestions().then(function(data){
        $scope.questions = [];
        $scope.nexPage = false;
        $scope.manupulateQuestions(data);
      },function(err){
        console.log(err);
      })
    };

var chart = AmCharts.makeChart("chartdiv", {
  "type": "serial",
  "theme": "light",
  "titles": [{
    "text": "Country"
  }, {
    "text": "My Chart Sub-Title",
    "bold": true
  }],
  "dataProvider": [{
    "country": "USA",
    "visits": 2025
  }, {
    "country": "China",
    "visits": 1882
  }, {
    "country": "Japan",
    "visits": 1809
  }, {
    "country": "Germany",
    "visits": 1322
  }, {
    "country": "UK",
    "visits": 1122
  }, {
    "country": "France",
    "visits": 1114
  }, {
    "country": "India",
    "visits": 984
  }, {
    "country": "Spain",
    "visits": 711
  }, {
    "country": "Netherlands",
    "visits": 665
  }, {
    "country": "Russia",
    "visits": 580
  }, {
    "country": "South Korea",
    "visits": 443
  }, {
    "country": "Canada",
    "visits": 441
  }, {
    "country": "Brazil",
    "visits": 395
  }],
  "valueAxes": [{
    "gridColor": "#FFFFFF",
    "gridAlpha": 0.2,
    "dashLength": 0
  }],
  "gridAboveGraphs": true,
  "startDuration": 1,
  "graphs": [{
    "balloonText": "[[category]]: <b>[[value]]</b>",
    "fillAlphas": 0.8,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits"
  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "gridAlpha": 0
  }
});

var chart = AmCharts.makeChart("chartdiv1", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [{
    "country": "Lithuania",
    "litres": 901.9
  }, {
    "country": "Czech Republic",
    "litres": 301.9
  }, {
    "country": "Ireland",
    "litres": 201.1
  }, {
    "country": "Germany",
    "litres": 165.8
  }, {
    "country": "Australia",
    "litres": 139.9
  }, {
    "country": "Austria",
    "litres": 128.3
  }, {
    "country": "UK",
    "litres": 99
  }, {
    "country": "Belgium",
    "litres": 60
  }, {
    "country": "The Netherlands",
    "litres": 50
  }],
  "valueField": "litres",
  "titleField": "country",
  "balloon": {
    "fixedPosition": true
  },
  "export": {
    "enabled": true,
    "menu": []
  }
});

var chart = AmCharts.makeChart("chartdiv2", {
    "theme": "light",
    "type": "serial",
    "dataProvider": [{
        "country": "USA",
        "year2004": 3.5,
        "year2005": 4.2
    }, {
        "country": "UK",
        "year2004": 1.7,
        "year2005": 3.1
    }, {
        "country": "Canada",
        "year2004": 2.8,
        "year2005": 2.9
    }, {
        "country": "Japan",
        "year2004": 2.6,
        "year2005": 2.3
    }, {
        "country": "France",
        "year2004": 1.4,
        "year2005": 2.1
    }, {
        "country": "Brazil",
        "year2004": 2.6,
        "year2005": 4.9
    }, {
        "country": "Russia",
        "year2004": 6.4,
        "year2005": 7.2
    }, {
        "country": "India",
        "year2004": 8,
        "year2005": 7.1
    }, {
        "country": "China",
        "year2004": 9.9,
        "year2005": 10.1
    }],
    "valueAxes": [{
        "stackType": "3d",
        "unit": "%",
        "position": "left",
        "title": "GDP growth rate",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "GDP grow in [[category]] (2004): <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "year2004"
    }, {
        "balloonText": "GDP grow in [[category]] (2005): <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "valueField": "year2005"
    }],
    "plotAreaFillAlphas": 0.1,
    "depth3D": 60,
    "angle": 30,
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});

var chart = AmCharts.makeChart("chartdiv3", {
  "type": "pie",
  "startDuration": 0,
   "theme": "light",
  "addClassNames": true,
  "legend":{
   	"position":"right",
    "marginRight":100,
    "autoMargins":false
  },
  "innerRadius": "30%",
  "defs": {
    "filter": [{
      "id": "shadow",
      "width": "200%",
      "height": "200%",
      "feOffset": {
        "result": "offOut",
        "in": "SourceAlpha",
        "dx": 0,
        "dy": 0
      },
      "feGaussianBlur": {
        "result": "blurOut",
        "in": "offOut",
        "stdDeviation": 5
      },
      "feBlend": {
        "in": "SourceGraphic",
        "in2": "blurOut",
        "mode": "normal"
      }
    }]
  },
  "dataProvider": [{
    "country": "Lithuania",
    "litres": 501.9
  }, {
    "country": "Czech Republic",
    "litres": 301.9
  }, {
    "country": "Ireland",
    "litres": 201.1
  }, {
    "country": "Germany",
    "litres": 165.8
  }, {
    "country": "Australia",
    "litres": 139.9
  }, {
    "country": "Austria",
    "litres": 128.3
  }, {
    "country": "UK",
    "litres": 99
  }, {
    "country": "Belgium",
    "litres": 60
  }, {
    "country": "The Netherlands",
    "litres": 50
  }],
  "valueField": "litres",
  "titleField": "country",
  "export": {
    "enabled": true
  }
});


var chart = AmCharts.makeChart("chartdiv4", {
    "theme": "light",
    "type": "serial",
    "dataProvider": [{
        "year": 2005,
        "income": 23.5
    }, {
        "year": 2006,
        "income": 26.2
    }, {
        "year": 2007,
        "income": 30.1
    }, {
        "year": 2008,
        "income": 29.5
    }, {
        "year": 2009,
        "income": 24.6
    }],
    "valueAxes": [{
        "title": "Income in millions, USD"
    }],
    "graphs": [{
        "balloonText": "Income in [[category]]:[[value]]",
        "fillAlphas": 1,
        "lineAlpha": 0.2,
        "title": "Income",
        "type": "column",
        "valueField": "income"
    }],
    "depth3D": 20,
    "angle": 30,
    "rotate": true,
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "fillAlpha": 0.05,
        "position": "left"
    },
    "export": {
    	"enabled": true
     }
});


var chart = AmCharts.makeChart("chartdiv5", {
    "theme": "light",
    "type": "serial",
	"startDuration": 2,
    "dataProvider": [{
        "country": "USA",
        "visits": 4025,
        "color": "#FF0F00"
    }, {
        "country": "China",
        "visits": 1882,
        "color": "#FF6600"
    }, {
        "country": "Japan",
        "visits": 1809,
        "color": "#FF9E01"
    }, {
        "country": "Germany",
        "visits": 1322,
        "color": "#FCD202"
    }, {
        "country": "UK",
        "visits": 1122,
        "color": "#F8FF01"
    }, {
        "country": "France",
        "visits": 1114,
        "color": "#B0DE09"
    }, {
        "country": "India",
        "visits": 984,
        "color": "#04D215"
    }, {
        "country": "Spain",
        "visits": 711,
        "color": "#0D8ECF"
    }, {
        "country": "Netherlands",
        "visits": 665,
        "color": "#0D52D1"
    }, {
        "country": "Russia",
        "visits": 580,
        "color": "#2A0CD0"
    }, {
        "country": "South Korea",
        "visits": 443,
        "color": "#8A0CCF"
    }, {
        "country": "Canada",
        "visits": 441,
        "color": "#CD0D74"
    }, {
        "country": "Brazil",
        "visits": 395,
        "color": "#754DEB"
    }, {
        "country": "Italy",
        "visits": 386,
        "color": "#DDDDDD"
    }, {
        "country": "Australia",
        "visits": 384,
        "color": "#999999"
    }, {
        "country": "Taiwan",
        "visits": 338,
        "color": "#333333"
    }, {
        "country": "Poland",
        "visits": 328,
        "color": "#000000"
    }],
    "valueAxes": [{
        "position": "left",
        "title": "Visitors"
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "visits"
    }],
    "depth3D": 20,
	"angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 90
    },
    "export": {
    	"enabled": true
     }

});

var chart = AmCharts.makeChart("chartdiv6", {
    "type": "serial",
    "theme": "light",
    "legend": {
        "equalWidths": false,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 120
    },
    "dataProvider": [{
        "date": "2012-01-01",
        "distance": 227,
        "townName": "New York",
        "townName2": "New York",
        "townSize": 25,
        "latitude": 40.71,
        "duration": 408
    }, {
        "date": "2012-01-02",
        "distance": 371,
        "townName": "Washington",
        "townSize": 14,
        "latitude": 38.89,
        "duration": 482
    }, {
        "date": "2012-01-03",
        "distance": 433,
        "townName": "Wilmington",
        "townSize": 6,
        "latitude": 34.22,
        "duration": 562
    }, {
        "date": "2012-01-04",
        "distance": 345,
        "townName": "Jacksonville",
        "townSize": 7,
        "latitude": 30.35,
        "duration": 379
    }, {
        "date": "2012-01-05",
        "distance": 480,
        "townName": "Miami",
        "townName2": "Miami",
        "townSize": 10,
        "latitude": 25.83,
        "duration": 501
    }, {
        "date": "2012-01-06",
        "distance": 386,
        "townName": "Tallahassee",
        "townSize": 7,
        "latitude": 30.46,
        "duration": 443
    }, {
        "date": "2012-01-07",
        "distance": 348,
        "townName": "New Orleans",
        "townSize": 10,
        "latitude": 29.94,
        "duration": 405
    }, {
        "date": "2012-01-08",
        "distance": 238,
        "townName": "Houston",
        "townName2": "Houston",
        "townSize": 16,
        "latitude": 29.76,
        "duration": 309
    }, {
        "date": "2012-01-09",
        "distance": 218,
        "townName": "Dalas",
        "townSize": 17,
        "latitude": 32.8,
        "duration": 287
    }, {
        "date": "2012-01-10",
        "distance": 349,
        "townName": "Oklahoma City",
        "townSize": 11,
        "latitude": 35.49,
        "duration": 485
    }, {
        "date": "2012-01-11",
        "distance": 603,
        "townName": "Kansas City",
        "townSize": 10,
        "latitude": 39.1,
        "duration": 890
    }, {
        "date": "2012-01-12",
        "distance": 534,
        "townName": "Denver",
        "townName2": "Denver",
        "townSize": 18,
        "latitude": 39.74,
        "duration": 810
    }, {
        "date": "2012-01-13",
        "townName": "Salt Lake City",
        "townSize": 12,
        "distance": 425,
        "duration": 670,
        "latitude": 40.75,
        "dashLength": 8,
        "alpha": 0.4
    }, {
        "date": "2012-01-14",
        "latitude": 36.1,
        "duration": 470,
        "townName": "Las Vegas",
        "townName2": "Las Vegas"
    }, {
        "date": "2012-01-15"
    }, {
        "date": "2012-01-16"
    }, {
        "date": "2012-01-17"
    }, {
        "date": "2012-01-18"
    }, {
        "date": "2012-01-19"
    }],
    "valueAxes": [{
        "id": "distanceAxis",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left",
        "title": "distance"
    }, {
        "id": "latitudeAxis",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "labelsEnabled": false,
        "position": "right"
    }, {
        "id": "durationAxis",
        "duration": "mm",
        "durationUnits": {
            "hh": "h ",
            "mm": "min"
        },
        "axisAlpha": 0,
        "gridAlpha": 0,
        "inside": true,
        "position": "right",
        "title": "duration"
    }],
    "graphs": [{
        "alphaField": "alpha",
        "balloonText": "[[value]] miles",
        "dashLengthField": "dashLength",
        "fillAlphas": 0.7,
        "legendPeriodValueText": "total: [[value.sum]] mi",
        "legendValueText": "[[value]] mi",
        "title": "distance",
        "type": "column",
        "valueField": "distance",
        "valueAxis": "distanceAxis"
    }, {
        "balloonText": "latitude:[[value]]",
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "useLineColorForBulletBorder": true,
        "bulletColor": "#FFFFFF",
        "bulletSizeField": "townSize",
        "dashLengthField": "dashLength",
        "descriptionField": "townName",
        "labelPosition": "right",
        "labelText": "[[townName2]]",
        "legendValueText": "[[value]]/[[description]]",
        "title": "latitude/city",
        "fillAlphas": 0,
        "valueField": "latitude",
        "valueAxis": "latitudeAxis"
    }, {
        "bullet": "square",
        "bulletBorderAlpha": 1,
        "bulletBorderThickness": 1,
        "dashLengthField": "dashLength",
        "legendValueText": "[[value]]",
        "title": "duration",
        "fillAlphas": 0,
        "valueField": "duration",
        "valueAxis": "durationAxis"
    }],
    "chartCursor": {
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
         "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
    },
    "dataDateFormat": "YYYY-MM-DD",
    "categoryField": "date",
    "categoryAxis": {
        "dateFormats": [{
            "period": "DD",
            "format": "DD"
        }, {
            "period": "WW",
            "format": "MMM DD"
        }, {
            "period": "MM",
            "format": "MMM"
        }, {
            "period": "YYYY",
            "format": "YYYY"
        }],
        "parseDates": true,
        "autoGridCount": false,
        "axisColor": "#555555",
        "gridAlpha": 0.1,
        "gridColor": "#FFFFFF",
        "gridCount": 50
    },
    "export": {
    	"enabled": true
     }
});


function exportChart() {
  chart["export"].capture({}, function() {

    // SAVE TO PNG
    this.toPNG({}, function(base64) {

      // We have now a Base64-encoded image data
      // which we can now transfer to server via AJAX
      // i.e. jQuery.post( "saveimage.php", { "data": base64 } )
      console.log(base64);
    });
  });
}

    $scope.loadJs = function(){
    	//stick in the fixed 100% height behind the navbar but don't wrap it
	    $('#slide-nav.navbar .container-flued').append($('<div id="navbar-height-col"></div>'));

	    // Enter your ids or classes
	    var toggler = '.navbar-toggle';
	    var pagewrapper = '#page-content';
	    // var navigationwrapper = '.navbar-header';
	    var menuwidth = '100%'; // the menu inside the slide menu itself
	    var slidewidth = '50%';
	    var menuneg = '-100%';
	    var slideneg = '-80%';


	    $("#slide-nav").on("click", toggler, function (e) {

	        var selected = $(this).hasClass('slide-active');

	        $('#slidemenu').stop().animate({
	            right: selected ? menuneg : '0px'
	        });

	        $('#navbar-height-col').stop().animate({
	            right: selected ? slideneg : '0px'
	        });

	        $(pagewrapper).stop().animate({
	            right: selected ? '0px' : slidewidth
	        });

	        // $(navigationwrapper).stop().animate({
	        //     right: selected ? '0px' : slidewidth
	        // });
	        console.log(slidewidth);

	        $(this).toggleClass('slide-active', !selected);
	        $('#slidemenu').toggleClass('slide-active');


	        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');


	    });


	    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';


	    $(window).on("resize", function () {

	        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
	            $(selected).removeClass('slide-active');
	        }


	    });
		

    };
  });
