'use strict';

/**
 * @ngdoc function
 * @name projectsApp.controller:ModalCtrl
 * @description
 * # ModalCtrl
 * Controller of the projectsApp
 */
angular.module('voteApp')
  .controller('ModalCtrl', function ($scope,$rootScope,$uibModal,userService,$window) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.login = function(user){
        $scope.loader = true;
    	userService.login(user).then(function(res){
            $window.localStorage.setItem('token',res.token);
            $window.localStorage.setItem('usertype',res.role);
            $window.localStorage.setItem('name',res.first_name);
            $rootScope.name = res.first_name;
            $rootScope.login=true;
    		$scope.close();
            $scope.loader = false;
    	},function(err){
             $scope.loader = false;
    		$rootScope.showCustomToast('Email or Password is Invalid');
    	})
    };
    $scope.signUp = function(user){
        $scope.loader = true;
    	user.role = 'user';
    	userService.signup(user).then(function(res){
            $window.localStorage.setItem('token',res.token);
            $window.localStorage.setItem('usertype',res.role);
            $window.localStorage.setItem('name',res.first_name);
            $rootScope.name = res.first_name;
            $rootScope.login=true;
            $scope.loader = false;
    		$scope.close();
            $rootScope.showCustomToast('Successfully Registered Please Login');
    	},function(err){
            $scope.loader = false;
            console.log(err)
    		$rootScope.showCustomToast(err);
    	})
    };
    $scope.close = function(){
        $scope.loader = false;
    	$rootScope.modalInstance.close();
    };
    $scope.questionCreate = function(data){
        $scope.loader = true;
        data.category_id = 1;
        userService.questionCreate(data).then(function(res){
            $scope.loader = false;
            $scope.close();
        },function(err){
            $scope.loader = false;
            $scope.close();
            $rootScope.showCustomToast(err);
        })
    };

    $scope.commentCreate = function(data){
        $scope.loader = true;
        userService.comment(data).then(function(res){
            $rootScope.showCustomToast('You successfully posted your comment.');
            $scope.comments = res;
            $scope.loader = false;
            // $scope.close();
        },function(err){
            $scope.close();
            $scope.loader = false;
            $rootScope.showCustomToast(err);
        })
    }
    $scope.forgotPassword = function(data){
        $scope.loader = true;
        userService.forgotPassword(data).then(function(res){
            $rootScope.showCustomToast('You successfully sent email.');
            $scope.comments = res;
            $scope.loader = false;
            // $scope.close();
        },function(err){
            $scope.close();
            $scope.loader = false;
            $rootScope.showCustomToast(err);
        })
    }

  });
