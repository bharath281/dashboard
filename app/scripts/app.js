'use strict';

/**
 * @ngdoc overview
 * @name voteApp
 * @description
 * # voteApp
 *
 * Main module of the application.
 */
angular
  .module('voteApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ngMaterial',
    'ngMessages',
    '720kb.socialshare'
  ])
  .config(function ($routeProvider,$httpProvider,$locationProvider) {
    $httpProvider.interceptors.push('httpinterceptor');

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
	  
      // $locationProvider.html5Mode(true);

  })
  .run(function($rootScope,$mdToast){
    $rootScope.showCustomToast = function(message) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(message)
          .hideDelay(3000)
          .position('top right')
          .action('ok')
      );
    };

  })
  .constant('API_END_POINT','http://api.brigadepoll.com/api/')
  .factory('httpinterceptor', ['$q','$location', function($q,$location,$rootScope,$window) {  
      return {
          responseError: function(response) {
             if (response.status === 401 || response.data.status === 401){
                  console.log(response);
                  localStorage.clear();
                  $rootScope.showCustomToast('Please relogin to proceed.')
                  $location.path('/'); 
                  $rootScope.login=false;
                  $rootScope.name = '';
                  $window.location.reload();
              }
              return $q.reject(response);
          },
          request: function (config) {
            console.log(config);
             config.headers = config.headers || {};
             if (window.localStorage && localStorage.getItem('token')) {
                 var token = localStorage.getItem('token');
                 config.headers.Authorization = token;
             }
             return config;
         },
      };
  }]);
