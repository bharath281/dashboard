'use strict';

/**
 * @ngdoc service
 * @name voteApp.userService
 * @description
 * # userService
 * Service in the voteApp.
 */
angular.module('voteApp')
  .service('userService', function ($q, $http, API_END_POINT) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.signup = function (user) { 
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: API_END_POINT+'authentication/register', 
            accepts: "application/json; charset=utf-8",
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };

    this.login = function (user) { 
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: API_END_POINT+'authentication/login', 
            accepts: "application/json; charset=utf-8",
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };

    this.getQuesions = function (page) { 
        var deferred = $q.defer();
        var end = '';
        if(page){
            end = '?page='+page;
        }
        $http({
            method: 'GET',
            url: API_END_POINT+'questions'+end, 
            accepts: "application/json; charset=utf-8",
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };

    this.getMenu = function (user) { 
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: API_END_POINT+'category/list', 
            accepts: "application/json; charset=utf-8",
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };
    this.getMenuQuestions = function (id) { 
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: API_END_POINT+'questions/category/'+id, 
            accepts: "application/json; charset=utf-8",
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };
    this.getMyQuestions = function (id) { 
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: API_END_POINT+'questions/myquestions', 
            accepts: "application/json; charset=utf-8",
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };

    this.questionCreate = function (user) { 
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: API_END_POINT+'questions/create', 
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };

    this.vote = function (data) { 
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: API_END_POINT+'vote', 
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };

    this.comment = function (data) { 
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: API_END_POINT+'comment', 
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };

    this.getComments = function (id) { 
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: API_END_POINT+'comment/'+id, 
            accepts: "application/json; charset=utf-8",
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };

     this.forgotPassword = function (data) { 
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: API_END_POINT+'authentication/forgot', 
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
               deferred.resolve(response.data);
            },function(data){
               deferred.reject(data.statusText);
            });

        return deferred.promise;
    };


  });
