'use strict';

/**
 * @ngdoc directive
 * @name projectsApp.directive:questionDirective
 * @description
 * # questionDirective
 */
angular.module('voteApp')
  .directive('questionDirective', function (userService,$rootScope,Socialshare,$window) {
    return {
      templateUrl: 'views/questions.html',
      restrict: 'E',
      scope:{
      	question:'=question',
        commentfn:'&commentFn',
        viewcomment:'&viewcommentFn',
        loginfn:'&loginFn'
      },
      link: function postLink(scope, element, attrs) {

  	    scope.vote = function(ans){
          if(ans){
    	  	  if($rootScope.login){
    		      userService.vote({question_id:scope.question.id,answer:ans}).then(function(data){
    		      	scope.question.votes = data;
    		      },function(err){
                $rootScope.login=false;
                $window.location.reload();
    		      	$rootScope.showCustomToast(err);
    		      })
    	      }else{
    	      	// $rootScope.showCustomToast('Please login to vote a question.');
              scope.loginfn();
    	      }
          }else{
            $rootScope.showCustomToast('Please choose answer.');
          }
  	    };

        scope.getComments=function(){
          scope.viewcomment(scope.id);
        };

        scope.share = function(){
          // Socialshare.share({
          //   'provider': 'facebook',
          //   'attrs': {
          //     'socialshareUrl': 'http://720kb.net',
          //     'socialshareText':"test"
          //   }
          // });
            FB.ui(
            {
                method: 'feed',
                name: scope.question.question,
                link: 'http://brigadepoll.com/',
                picture: 'http://api.brigadepoll.com/file/11.png',
                caption: 'Question',
                description: 'click following link to answer the question http://brigadepoll.com/'
            });
        };
        scope.sharetwitter = function(){
          // Socialshare.share({
          //   'provider': 'facebook',
          //   'attrs': {
          //     'socialshareUrl': 'http://720kb.net',
          //     'socialshareText':"test"
          //   }
          // });
            FB.ui(
            {
                method: 'feed',
                name: scope.question.question,
                link: 'http://brigadepoll.com/',
                picture: 'http://api.brigadepoll.com/file/11.png',
                caption: 'Question',
                description: 'click following link to answer the question http://brigadepoll.com/'
            });
        };

        scope.comment = function(){
          //scope.commentfn(scope.id);
          scope.viewcomment(scope.id);
          // console.log('print id',scope.id);
        };

      }
    };
  });
